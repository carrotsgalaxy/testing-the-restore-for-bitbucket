var socket = io(getUrl());

socket.emit("join_room_request", {room : ticket});

$('#message').on('keydown', function(e) {
    if (e.which == 13) {
      var val = $('#message').val();
      if(val == "") {
        $('#socket').addClass('error');
        $('#socket').text("Please input a message!");
      } else {
        socket.emit('new_message', {username: username, message: val, ticket: ticket})
      }
    }
});

socket.emit('get_messages', {ticket: ticket});

socket.on('get_messages', function(data) {
  for(var i = 0; i < data.messages.length; i++) {
    obj = data.messages[i];
    var date = lastMessageCalculator(twoDates(new Date(), new Date(obj.date)));
    $('.chat_messages')
    .append("<div class='message'><div class='message_head'>" + obj.username + " <span class='message_date'>"+date+"</span></div><div class='message_content'>" + obj.message + "</div></div>");
  }
  $(".chat_messages").scrollTop($(".chat_messages")[0].scrollHeight);
});

socket.on('broadcast_message', function(data) {
  $('.chat_messages')
  .append("<div class='message'><div class='message_head'>" + data.username + " <span class='message_date'>Just now...</span></div><div class='message_content'>" + data.message + "</div></div>");
  $(".chat_messages").scrollTop($(".chat_messages")[0].scrollHeight);
});

socket.on('error', function (data) {
  $('#socket').addClass('error');
  $('#socket').text(data.message);
});

socket.on('success', function (data) {
  $('#socket').addClass('success');
  $('#socket').text(data.message);
  $('#message').val("");
});
