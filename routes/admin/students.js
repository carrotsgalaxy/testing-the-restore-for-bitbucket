var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var year = require('../utils/year');

router.get('/',function(req,res){
  utils.isLoggedIn(req,function(call) {
    if(call == false) {
      res.redirect('/');
    } else {
      utils.getUserData(req.session.uuid, function(obj) {
        if(obj == 1) {
          console.log("Couldn't find data for " + req.session.uuid);
        } else {
          utils.getGroupByUUID(req.session.uuid, function(g) {
            utils.getAllStudents(function(students) {
              if(g.admin == false) {
                req.flash('error', 'Something went wrong...')
                res.redirect("/error");
              } else {
                res.render('admin/students', {
                  admin: g.admin,
                  user: obj,
                  group: g,
                  students: students,
                  year_utils: year,
                  active: "All Students",
                });
              }
            });
          });
        }
      });
    }
  });
});

module.exports = router;
