var express = require('express');
var router = express.Router();
var utils = require('./utils/utils');


router.get('/',function(req,res){
  res.render('auth/error', {
    message: req.flash('message')
  })
});

module.exports = router;
