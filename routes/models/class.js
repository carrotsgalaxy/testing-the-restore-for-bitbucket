var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var classSchema = new Schema({
  class: { type: String, required: true },
  subject: { type: String, required: true },
  name: {type: String, required: true},
  created_at: Date,
  updated_at: Date
});

classSchema.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();

  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});
classSchema.set('collection', 'classes');

var User = mongoose.model('Class', classSchema);

// make this available to our users in our Node applications
module.exports = User;
