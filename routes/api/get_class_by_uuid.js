var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var bcrypt = require('bcrypt-nodejs');
router.get('/get_class_by_uuid/:uuid',function(req,res){
    utils.getClassByUUID(req.params.uuid, function(call) {
      if(call == 1) {
        res.send({"response" : 1, "message" : "Invalid uuid!"});
      } else {
        res.send(call);
      }
    });
});

module.exports = router;
