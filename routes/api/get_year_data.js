var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var bcrypt = require('bcrypt-nodejs');
router.get('/get_year_data/:uuid',function(req,res){
  utils.getStudentData(req.params.uuid, function(data) {
    if(data == 1) {
      res.send({"response" : 1, "message" : "Invalid user!"});
    } else {
      utils.getYearData(data.year, function(call) {
        if(call == 1) {
          res.send({"response" : 1, "message" : "Couldn't find year data!"});
        } else {
          res.send(call);
        }
      });
    }
  });
});

module.exports = router;
