var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var bcrypt = require('bcrypt-nodejs');
router.get('/get_user_data/:id',function(req,res){
    utils.getUserData(req.params.id, function(call) {
      if(call == 1) {
        res.send({"response" : 1, "message" : "Invalid user!"});
      } else {
        res.send(call);
      }
    });
});

module.exports = router;
