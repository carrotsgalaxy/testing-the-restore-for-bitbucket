var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');

router.get('/get_all_students',function(req,res){
  utils.getAllStudents(function(callback) {
    if(callback == 1) {
      res.send({"response" : 1, "message" : "No students are currently avaliable!"});
    } else {
      res.send(callback);
    }
  });
});

module.exports = router;
